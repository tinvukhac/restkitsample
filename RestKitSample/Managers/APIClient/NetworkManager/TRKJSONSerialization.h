//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import <RestKitSANetworking@MindSea/RestKit.h>

@interface TRKJSONSerialization : NSObject <RKSerialization>

@end
