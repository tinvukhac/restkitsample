//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import "TRKJSONSerialization.h"

@implementation TRKJSONSerialization

+ (id)objectFromData:(NSData *)data error:(NSError **)error
{
    NSLog(@"Response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
}

+ (NSData *)dataFromObject:(id)object error:(NSError **)error
{
    NSLog(@"Request: %@", object);
    return [NSJSONSerialization dataWithJSONObject:object options:0 error:error];
}

@end
