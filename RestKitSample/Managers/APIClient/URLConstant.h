//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLConstant : NSObject

extern NSString* const BASE_URL;
extern NSString* const SESSION;
extern NSString* const USER;
extern NSString* const DRIVER;

@end
