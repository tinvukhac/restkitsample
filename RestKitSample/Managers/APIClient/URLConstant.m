//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import "URLConstant.h"

@implementation URLConstant

NSString * const BASE_URL                        =   @"http://dev2.api.123xe.vn"; //http://dev.mapi2.me.zing.vn http://api.timxekhach.com
NSString * const SESSION                         =   @"/uodapi/session";
NSString * const USER                            =   @"/uodapi/user";
NSString * const DRIVER                          =   @"/uodapi/driver";

@end
