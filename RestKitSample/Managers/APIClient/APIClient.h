//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKitSANetworking@MindSea/RestKit.h>
#import "SAHTTPClient.h"
#import "SAImageRequestOperation.h"
#import "ResponseObject.h"

#define RESTKIT_FAILURE -1

@interface APIClient : SAHTTPClient

#pragma mark- Public methods

+ (APIClient *)sharedClient;

//Define all API functions here

#pragma mark- I. Session
#pragma mark- 1.1. Register

- (void)registerWithUsername:(NSString *)username
                    password:(NSString *)password
                       block:(void (^)(ResponseObject * responseObject))block;

@end
