//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import "APIClient.h"
#import "URLConstant.h"
#import "AppDelegate.h"

#define API_VERSION @"1.01"
#define API_VERSION_KEY @"api_ver"
#define SESSION_KEY @"sessionkey"

// Objects
#import "RegisterResult.h"

@implementation APIClient

#pragma mark- Public methods

+ (APIClient *)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[SAHTTPRequestOperation class]];
    
    return self;
}

//Implement all API functions here

#pragma mark- I. Session
#pragma mark- 1.1. Register

-(void) registerWithUsername:(NSString *)username password:(NSString *)password block:(void (^)(ResponseObject *))block{
    RKResponseDescriptor * responseDescriptor = [self setupResponseObjectWithResult:[RegisterResult class]
                                                                 attributeMappings:@{
                                                                                     @"json_property1":@"property1",
                                                                                     @"json_property2":@"property2",
                                                                                     @"json_property3":@"property3",
                                                                                     }
                                                                       pathPattern:SESSION];
    
    NSMutableDictionary * params = [[NSMutableDictionary alloc] init];
    [params setObject:@"param1_value" forKey:@"param1_key"];
    [params setObject:@"param2_value" forKey:@"param2_key"];
    NSDictionary *parameters = [NSDictionary dictionaryWithDictionary:params];
    
    [self postWithPath:SESSION parameters:parameters responseDescriptor:responseDescriptor block:block];
}

#pragma mark - Private methods

- (RKResponseDescriptor *)setupResponseObjectMapping:(Class)responseClass
                                   attributeMappings:(NSDictionary *)responseAttributeMapping
                                         resultClass:(Class)resultClass
                                   attributeMappings:(NSDictionary *)resultAttributeMapping
                                         pathPattern:(NSString *)pathPattern
{
    RKObjectMapping * responseObjectMapping = [RKObjectMapping mappingForClass:responseClass];
    [responseObjectMapping addAttributeMappingsFromDictionary:responseAttributeMapping];
    
    if (resultClass && resultAttributeMapping) {
        RKObjectMapping * resultObjectMapping = [RKObjectMapping mappingForClass:resultClass];
        [resultObjectMapping addAttributeMappingsFromDictionary:resultAttributeMapping];
        
        [responseObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"result" withMapping:resultObjectMapping]];
    }
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseObjectMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:pathPattern
                                                                                           keyPath:@""
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    return responseDescriptor;
}

- (RKResponseDescriptor*)setupResponseObjectWithResult:(Class)resultClass
                                     attributeMappings:(NSDictionary *)resultAttributeMapping
                                           pathPattern:(NSString *)pathPattern {
    RKObjectMapping * responseObjectMapping = [RKObjectMapping mappingForClass:[ResponseObject class]];
    [responseObjectMapping addAttributeMappingsFromDictionary:@{
                                                                @"error_code": @"errorCode",
                                                                @"error_message": @"message",
                                                                }];
    
    if (resultClass && resultAttributeMapping) {
        RKObjectMapping * resultObjectMapping = [RKObjectMapping mappingForClass:resultClass];
        [resultObjectMapping addAttributeMappingsFromDictionary:resultAttributeMapping];
        
        [responseObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"result" withMapping:resultObjectMapping]];
    }
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseObjectMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:pathPattern
                                                                                           keyPath:@""
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    return responseDescriptor;
}

- (RKResponseDescriptor *)setupResponseObjectWithArrayName:(NSString *)arrayName
                                            arrayItemClass:(Class)arrayItemClass
                                arrayItemAttributeMappings:(NSDictionary *)arrayItemAttributeMappings
                                               pathPattern:(NSString *)pathPattern
{
    RKObjectMapping * responseObjectMapping = [RKObjectMapping mappingForClass:[ResponseObject class]];
    [responseObjectMapping addAttributeMappingsFromDictionary:@{
                                                                @"error_code": @"errorCode",
                                                                @"error_message": @"message",
                                                                }];
    
    RKObjectMapping * arrayItemObjectMapping = [RKObjectMapping mappingForClass:[arrayItemClass class]];
    [arrayItemObjectMapping addAttributeMappingsFromDictionary:arrayItemAttributeMappings];
    
    [responseObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:[NSString stringWithFormat:@"data.%@", arrayName] toKeyPath:@"result" withMapping:arrayItemObjectMapping]];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseObjectMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:pathPattern
                                                                                           keyPath:@""
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    return responseDescriptor;
}

- (RKResponseDescriptor *)setupResponseObjectWithArrayName:(NSString *)arrayName
                                            arrayItemClass:(Class)arrayItemClass
                                arrayItemAttributeMappings:(NSDictionary *)arrayItemAttributeMappings
                                           nestedArrayName:(NSString *)nestedArrayName
                                      nestedArrayItemClass:(Class)nestedArrayItemClass
                          nestedArrayItemAttributeMappings:(NSDictionary *)nestedArrayItemAttributeMappings
                                               pathPattern:(NSString *)pathPattern
{
    RKObjectMapping * responseObjectMapping = [RKObjectMapping mappingForClass:[ResponseObject class]];
    [responseObjectMapping addAttributeMappingsFromDictionary:@{
                                                                @"error_code": @"errorCode",
                                                                @"error_message": @"message",
                                                                }];
    
    RKObjectMapping * nestedArrayItemObjectMapping = [RKObjectMapping mappingForClass:[nestedArrayItemClass class]];
    [nestedArrayItemObjectMapping addAttributeMappingsFromDictionary:nestedArrayItemAttributeMappings];
    
    RKObjectMapping * arrayItemObjectMapping = [RKObjectMapping mappingForClass:[arrayItemClass class]];
    [arrayItemObjectMapping addAttributeMappingsFromDictionary:arrayItemAttributeMappings];
    
    [arrayItemObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:[NSString stringWithFormat:@"%@", nestedArrayName] toKeyPath:[NSString stringWithFormat:@"%@", nestedArrayName] withMapping:nestedArrayItemObjectMapping]];
    
    [responseObjectMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:[NSString stringWithFormat:@"data.%@", arrayName] toKeyPath:@"result" withMapping:arrayItemObjectMapping]];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseObjectMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:pathPattern
                                                                                           keyPath:@""
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    return responseDescriptor;
}

- (void)postWithPath:(NSString *)path
          parameters:(NSDictionary *)parameters
  responseDescriptor:(RKResponseDescriptor *)responseDescriptor
               block:(void (^)(ResponseObject * responseObject))block {
    
    [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    
    [[RKObjectManager sharedManager] postObject:nil
                                           path:path
                                     parameters:parameters
                                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                            ResponseObject * responseObject = [mappingResult.array objectAtIndex:0];
                                            [[RKObjectManager sharedManager] removeResponseDescriptor:responseDescriptor];
                                            block(responseObject);
                                        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                            [[RKObjectManager sharedManager] removeResponseDescriptor:responseDescriptor];
                                            ResponseObject * responseObject = [[ResponseObject alloc] init];
                                            responseObject.errorCode = RESTKIT_FAILURE;
                                            responseObject.message = error.localizedDescription;
                                            block(responseObject);
                                        }];
}

@end