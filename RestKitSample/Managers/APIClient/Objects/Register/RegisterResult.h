//
//  AppDelegate.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterResult : NSObject

@property (nonatomic, strong) NSString * property1;
@property (nonatomic, strong) NSString * property2;
@property (nonatomic, strong) NSString * property3;

@end
