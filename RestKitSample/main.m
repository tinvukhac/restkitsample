//
//  main.m
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
