//
//  AppDelegate.h
//  RestKitSample
//
//  Created by tinvukhac on 6/20/16.
//  Copyright © 2016 Indie Dev Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

#pragma mark- RestKit methods
- (void)configureRestKit;
- (void)configureRestKitWithUrl:(NSString *)url;

@end

